using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore;
using Newtonsoft.Json;

public class TaskQueueService
{
    private static readonly TaskQueueService instance = new TaskQueueService();
    private readonly Queue<TaskItem> taskQueue = new Queue<TaskItem>();
    private readonly object queueLock = new object();
    private bool isProcessing = false;
    public string responseString;
    private TaskQueueService() { }

    public static TaskQueueService Instance => instance;

    public async Task<string> EnqueueTaskAsync(TaskItem task)
    {
        lock (queueLock)
        {
            taskQueue.Enqueue(task);
            if (!isProcessing)
            {
                isProcessing = true;
                ProcessQueueAsync();
            }
        }
        await Task.CompletedTask;
        return responseString;
    }

    private async Task ProcessQueueAsync()
    {
        while (true)
        {
            TaskItem[] tasksToProcess;
            lock (queueLock)
            {
                if (taskQueue.Count == 0)
                {
                    isProcessing = false;
                    return;
                }

                tasksToProcess = new TaskItem[Math.Min(taskQueue.Count, 5)];
                for (int i = 0; i < tasksToProcess.Length; i++)
                {
                    tasksToProcess[i] = taskQueue.Dequeue();
                }
            }

            await Task.WhenAll(Array.ConvertAll(tasksToProcess, ProcessTaskAsync));
        }
    }

    private async Task ProcessTaskAsync(TaskItem task)
    {
        string filePath = "tasks.txt";
        string formattedTime = DateTime.UtcNow.ToString("o");
        string logEntry = $"{formattedTime} | {task.Message}";

        using (StreamWriter writer = File.AppendText(filePath))
        {
            writer.WriteLine(logEntry);
        }

        int processingTime = new Random().Next(50, 101);

        string responseTime = DateTime.UtcNow.ToString("o");
        int processingDuration = (int)(DateTime.Parse(responseTime) - DateTime.Parse(task.RequestTime)).Milliseconds;

        ApiResponse response = new ApiResponse
        {
            RequestTime = task.RequestTime,
            WriteTime = responseTime,
            ProcessingTime = processingDuration
        };

        string jsonResponse = JsonConvert.SerializeObject(response);
        responseString = jsonResponse;
        await Task.Delay(processingTime);

        await File.WriteAllTextAsync($"{task.Id}.json", jsonResponse);
    }
}

public class TaskItem
{
    public string Id { get; set; }
    public string RequestTime { get; set; }
    public string Message { get; set; }
}

public class ApiResponse
{
    public string RequestTime { get; set; }
    public string WriteTime { get; set; }
    public int ProcessingTime { get; set; }
}

public class Startup
{
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddControllers();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.EnvironmentName == "Development")
        {
            app.UseDeveloperExceptionPage();
        }

        app.UseRouting();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }
}

public class TaskController : Controller
{
    [HttpPost("/api/process")]
    public async Task<IActionResult> ProcessTask([FromBody] TaskItem task)
    {
        if (task == null || string.IsNullOrEmpty(task.Message) || task.Message.Length > 100)
        {
            return BadRequest("Invalid request format");
        }

        task.RequestTime = DateTime.UtcNow.ToString("o");
        task.Id = Guid.NewGuid().ToString();

        var ret = await TaskQueueService.Instance.EnqueueTaskAsync(task);

        return Ok(ret);
    }
}

public class Program
{
    public static void Main(string[] args)
    {
        CreateWebHostBuilder(args).Build().Run();
    }

    public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
        WebHost.CreateDefaultBuilder(args)
            .UseStartup<Startup>();
}
